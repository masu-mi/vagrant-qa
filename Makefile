.PHONY: setup

setup: ./provisioning/ansible-blog-server
	cd $< && gmake setup

./provisioning/ansible-blog-server:
	git clone git@bitbucket.org:masu_mi/ansible-blog-server.git $@
